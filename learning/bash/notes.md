# Bash guide
This is a summary of this [Bash Guide](http://s.ntnu.no/bashguide.pdf). 

## Summary
1. [Commands and Arguments](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#1---commands-and-arguments)
2. [Special Characters](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#2---special-characters)
3. [Parameters](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#3---parameters)
    1. [Special Parameters](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#31---special-parameters)
	2. [Variable Types](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#32---variable-types)
	3. [Parameter Expansion](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#33---parameter-expansion)
4. [Patterns](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#4---patterns)
	1. [Glob Patterns](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#41---glob-patterns)
	2. [Extended Globs](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#42---extended-globs)
	3. [Regular Expressions](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#43---regular-expressions)
	4. [Brace Expansion](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#44---brace-expansion)
5. [Test and Conditionals](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#5---test-and-conditionals)
	1. [Exit Status](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#51---exit-status)
	2. [Control Operators (&& and ||)](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#52---control-operators--and-)
	3. [Grouping Statements](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#53---grouping-statements)
	4. [Conditional Blocks (if, test)](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#54---conditional-blocks-if-test)
	5. [Conditional Loops (while, until and for)](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#55---conditional-loops-while-until-and-for)
	6. [Choices (case and select)](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#56---choices-case-and-select)
6. [Arrays](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#6---arrays)
	1. [Creating Arrays](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#61---creating-arrays)
	2. [Using Arrays](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#62---using-arrays)
	3. [Associative Arrays](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#63---associative-arrays)
7. [Input and Output](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#7---input-and-output)
	1. [Command-line Arguments](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#71---command-line-arguments)
	2. [The Environment](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#72---the-environment)
	3. [File Descriptors](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#73---file-descriptors)
	4. [Redirection](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#74---redirection)
8. [Ressources](https://github.com/Nairwolf/learning/blob/master/bash/notes.md#8---ressources)

## 1 - Commands and Arguments
* Bash divides each line into *words* that are demarcated by a whitespace character (spaces and tabs) 
* Arguments are separed from the command name and from each other by a whitespace
* echo "This is &nbsp; &nbsp; &nbsp; &nbsp; a &nbsp; &nbsp; test &nbsp; &nbsp; &nbsp; !"  
* Type `[ -f file ]` instead of `[-f file]`. Use quote `[ -f "my file" ]`
* `[` is a Bash builtin, while `[[` is a Bash keyword
* `[ a < b ]` doesn't work because Bash tries to redirect the file `b` to the command `[ a ]` 
* `[[ a < b ]]` does what we expect it to, alphabetical comparaison. 
* Add an *interpreter directive* (also called a *hashbang* or *shebang*) : `#!/bin/bash`
* Use `#!/usr/bin/env bash` for portability
* Don't use `/bin/sh` as the interpreter. **sh is not bash !** Bash is a "sh-compatible" shell but the opposite is not true. 

## 2 - Special Characters
* `" "` : *Whitespace* - Bash uses whitespace to determine words begin and end
* `$` : *Expansion* - Paramter expansion (`$var` or `${var}`), command substitution (`$(command)`), arithmetic expansion (`$((expression))`) 
* `''` : *Single quotes* - Protect the text inside, any kind of interpretation by Bash is ignored
* `""` : *Double quotes* - Protect the text inside, yet allow substitutions to occur
* `\` : *Escape* - Prevent the next character from being interpreted as a special character
* `#` : *Comment* - Comments aren ot processed by the shell 
* `[[]]` : *Test* - Evaluate a condition to determine wheter it is "true" or "false"
* `!` : *Negate* - Use to negate or revrse a text or exit status
* `><` : *Redirection* - Redirect a command's output or input
* `|` : *Pipe* - Redirect output from a initial command to the input of secondary command
* `;` : *Command separator* - Used to separate multiple commands that are on the same line
* `{}` : *Inline group* - Commands inside the curly braces are treated as if they were one command
* `()` : *Shubshell group* - 
* `(())` : *Arithmetic expression* - Used to evaluate arithmetic expression
* `$(())` : *Arithmetic expansion* - Comparable to the above, but the expression is replaced with the result of its arithmetic evaluation
* `~` : *Home directory* - If followed by a `/`, it means the current user's home directory, otherise a username will have to be specified

## 3 - Parameters
* Parameters come in tow flavors: *variables* and *special parameters*
* Special parameters are read-only, pre-set by Bash
* Variables are parameters that you can create and update yourself
* To store data in a variabl, use the *assignment* syntax : `varname=vardata`
* Do not write `varname = vardata` ! This is wrong ! 
* To access the data stored in a variable, we use **parameter expansion**. It's the substitution of a parameter by its value. 
* If `song="My song.mp3"`, don't use `rm $song`, but use `rm "$song"`

### 3.1 - Special Parameters
* **0**- Contains the name, or the path of the script
* **1 2 etc** - *Positional Parameters* contain the arguments that were passed to the current script or function
* \* - Expands to all the words of all the positional parameters
* **@** - Expands to all the words of all the positional parameters
* **#** - Expands to the number of positional parameters that are currently set
* **?** - Expands to the exit code of the most recently completed foreground command
* **$** - Expands to the PID of the current shell
* **!** - Expands to the PID of the command most recently executed in the background
* **_** - Expands to the last argument of the last command that was executed

* There is a lot of variables that the shell provides for you, see the manual for a comprehensive list

### 3.2 - Variable Types
Bash have a few different types of variables

* Array : `declare -a variable`
* Associative array : `declare -A variable`
* Integer : `declare -i variable`
* Read Only : `declare -r variable`
* Export : `declare -x variable`

### 3.3 - Parameter Expansion 
* `echo "'$USER', '$USERs', '${USER}s'"` returns : `'nairwolf', '', 'nairwolfs'`
* `for file in *.JPG *.jpeg do mv -- "$file" "${file%.*}.jpg" done` 
* The expression `${file%.*}` cuts off everything from the end starting with the last period (.)
* There is a lof of PE tricks available on the guide

## 4 - Patterns

* Pattern matchin serves two roles in the shell: selecting filenames within a directory, or determining wheter a string conforms to a desired format
* *globs* : Used to match a range of files, or to check variables against simple rules
* *extended globs* : More complicated expressions
* *regular expression* : Used to test user input or parse data. 

### 4.1 - Glob Patterns
* `*` : matches any string, including the null string
* `?` : matches any single character
* `[...]` : matches any one of the enclosed characters
* You should always use globs instead of `ls` to enumerate files. Globs will always expand safely and minimize the risk for bugs.

### 4.2 - Extended Globs
* Equivalent to regex. This feature is turned off by default, but can be turned on with the `shopt` command, which is used to toggle **sh**ell **op**tions : `shopt -s extglob`
* `?(list` : Matches zero or one occurence of the given patterns
* `*(list)` : Matches zero or more occurences of the given patterns
* `+(list)` : Matches one or more occurences of the given patterns
* `@(list)` : Matches one of the given patterns 
* `!(list)` : Matches anything but the given patterns
* The *list* inside the parentheses is a list of globs or extended globs separated by the `|` characther
* `echo !(*.jpg|*.bmp)` expands to anything that does not match the `*.jpg` or the `*.bmp`pattern.

### 4.3 - Regular Expressions
* Regex are similar to *Glob Patterns*, but they can only be used for pattern matching, not for filename matching
* Use the operator `=~` inside `[[` keyword. It matches the string that comes before it against the regex pattern that follows it. 

### 4.4 - Brace Expansion
* Globs only expand to actual filenames, but brace expansions will expand to any possible permutation of their contents
* `echo th{e,a}n` => `then than`
* It's possible to use brace expansion AND globs : `echo {/home/*,/root}/.*profile`
* Brace expansion happens before filename expansion
* `echo {1..9}` => `1 2 3 4 5 6 7 8 9`
* `echo {0,1}{0..9}` => `00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19`

## 5 - Test and Conditionals 

### 5.1 - Exit Status
* The exit code is like a return value from functions
* It's an integer between 0 and 255
* By convention, 0 is for success, any other number is for failure
* The special parameter `?` shows the exit code of the last foreground process that terminated

### 5.2 - Control Operators (&& and ||)
* Concept of *conditional execution*
* `&&` means AND and `||` means OR
* `&&` executes the second command if the first one was successfull
* `||` executes the second command if the first one was unsucessfull

### 5.3 - Grouping Statements
* Be careful when you're using too much control operators 
* `cmd1 && cmd2 && cmd3 || cmd4` can be dangerous. If cmd1 fails, the first `&&` tells to skip cmd2, then Bash sees `&&` and skips cmd3, Bash sees `||` and it execute cmd4
* `cmd1 && cmd2 && { cmd3 || cmd4; }` is usually what you want. 
* Don't forget semicolon before the closing curly brace ! 
* Command grouping can be used to redirect input to a group of statements instead just one
* `{ cmd1 cmd2 cmd3 } < file`. We're redirecting `file` to a group of commands that read input

### 5.4 - Conditional Blocks (if, test) 
* Different styles : 

```bash
if COMMANDS
then OTHER COMMANDS
fi
  ```

```bash
if COMMANDS
then
	OTHER COMMANDS
fi
```

```bash
if COMMANDS; then
	OTHER COMMANDS
fi
```
* Don't write that :
```bash
$ myname='Greg Wooledge' 
$ yourname='Someone Else'
$ [ $myname = $yourname ]
```

* But write this : 
```bash
$ [ "$myname" = "$yourname" ]
```

* `[[` is a shell keyword, and has features that `[` lacks
* *pattern matching* : `[[ $filename = *.png ]] && echo "$filename looks like a PNG file"`
* It helps with parameter expansions (no need quotes)

```bash
$ me='Greg Wooledge'
$ you='Someone Else'
$ [[ $me = $you ]] # It's fine
$ [ $me = $you ] # Error
```

* The `=` operator does pattern matching by default, whenever the *right-hand side* is **not** quoted

```bash
$ foo=[a-z]* name=nairwolf
$ [[ $name = $foo ]] && echo "Name $name matches pattern $foo"
Name nairwolf matches pattern [a-z]*
$ [[ $name = "$foo" ]] || echo "Name $name is not equal to the string $foo"
Name nairwolf is not equal to the string [a-z]*
```

* `<` and `>` have special significance in Bash 

```bash
$ [ apple < banana ] # Bash will try to find the file "banana"
$ [[ apple < banana ]] # Bash will return 0 as a success
```

* Comparaisons operators like `=`, `!=`, `>` and `<` treat their arguments as strings. 
Use *operators* to treat arguments as numbers. 
* Consult `man test` to consult operators supported by `test` and `[`
* `[` is better for POSIX portability, but `[[` supports more features

### 5.5 - Conditional Loops (while, until and for)
* `while CMD`: Repeat so long as command is executed successfully (exit code is 0)
* `until CMD`: Repeat so long as command is executed unsucessfully (exit code is not 0)
* `for VAR in WORDS`: Repeat the loop for each word, setting *variable* to each word in turn
* `for ((EXPR; EXPR; EXPR;))`: Starts by evaluating the first arithmetic expression; 
repeats the loop so long as the second arithmetic expression is successful; and at the end
of each loop evaluates the third arithmetic expression 

* Interesting example with `for` loop : 

```bash
$ for i in {10..1}
do 
	echo "$i"
done
``` 

Bash takes the characters between `in` and the end of the line, and splits them up into words.

Next examples will cause errors if you have a file like "my favorite song.mp3"

```bash
for file in $(ls *.mp3)
do 
	rm "$file"
done
# Bash will split `$(ls *.mp3)` into this `my favorite song.mp3` and it will try to remove
# `my` `favorite` `song.mp3`.
```

If you quote it, it will not work also. 

```bash
for file in "$(ls *.mp3)"
do 
	rm "$file"
done
# If you have two files `my favorite song.mp3` and `the worst song.mp3`, Bash will try
# to remove this file `my favorite song.mp3 the worst song.mp3`
```

In the second case, quotes will protect all whitespace from the output of `ls`.

The best solution is to use **globs**:

```bash
for file in *.mp3
do 
	rm "$file"
done
```

The result of expanding the glob is this : 
`for file in "my favorite song.mp3" "the worst song.mp3"`

* Interesting example with `while` : 

```bash
# Read input
while read -p $'What is your name ? ' name
do
	echo "Hello $name !"
done
```

```bash
# Check your mail every five minutes
while sleep 300
do
	kmail --check
done
```

```bash
# Wait for a host to come back online
while ! ping -c 1 -W 1 "$host"
do 
	echo "$host is still unavailable."
done; echo -e "$host is available again.\a"
```

* `until` loop is pretty much the same as `while`
* `continue` is used to skip the next iteration of a loop without executing the rest of 
the body
* `break` is used to jump out of the loop and continue with the script after it

### 5.6 - Choices (case and select)

* A `case` statement basically enumerates several possible *Glob Patterns* and checks the
content of your parameter against these

```bash
case $LANG in
	en*) echo 'Hello!' ;;
	fr*) echo 'Salut!' ;;
	de*) echo 'Guten Tag!' ;;
	C|POSIX) echo 'hello world' ;;
	*) echo 'I do not speak your language.' ;;
esac
```

* Each choice in a `case` statement consist of a pattern, a right parenthesis, a block of 
code, and two semi-colons to denote the end of the block of code
* A left parenthesis can be added to the left of the pattern
* `case` stop matching pattern as soon as one is successful. So we can use the `*` pattern
in the end to match any case that has not been caught by the other choices

* A `select` statemnt offers the user the choice of several options and executes a block 
of code with the user's choice in a parameter. The menu repeats until a `break` command is
executed

## 6 - Arrays
* Bash provides thre types of parameters: Strings, Integers and Arrays
* An array is a type of variable that **maps integers to strings**

### 6.1 - Creating Arrays
* `$ names=("Bob" "Peter" "$USER" "Big Bad John")`
* `$ names=([0]="Bob" [1]="Peter" [20]="$USER" [21]="Big Bad John")
* `$ names[0]="Bob"
* Array with holes in it is called a *sparse array*
* You can use *Globs* `$ photos=(~/"My photos"/*.jpg)`
* Do not use `ls` to create arrays with a bunch of filenames
```bash 
$ files=$(ls) # BAD ! It would create just one string with the output of ls
$ files=($(ls)) # BAD ! It would create an array, but it still splits up filenames with whitespace
$ files=(*) # GOOD !  
```

### 6.2 - Using Arrays

```bash
for file in "${myfiles[@]}";
do
	cp "$file" /backups/
done
```

* We use the quoted form `$"{myfiles[@]}"`, Bash replaces this syntax with each elements 
in the array properly quoted (like positional parameters are exanded)
* The following two examples have the same effect

```bash
names=("Bob" "Peter" "$USER" "Big Bag John)
for name in "${names[@]}"
do
	echo "$name"
done
```

```bash
for name in "Bob" "Peter" "$USER" "Big Bad John"
do
	echo "$name"
done
``` 

* Do not forget to **quote** the `${arrayname[@]}` expansion properly ! 
* You can expand single array elements by referencing their index : `${names[0]}`
* There is a second form of expanding all array elements. Use it only when you want to 
merge all your array elements into a single string

```bash
$ names=("Bob" "Peter" "$USER" "Big Bad John")
$ echo "Today's contestants are: ${names[*]}"
Today's contestants are: Bob Peter nairwolf Big Bad John
```

* You can also use the `printf` command

```bash
$ names=("Bob" "Peter" "$USER" "Big Bad John")
$ printf "%s\n" "${names[@]}"
Bob
Peter
nairwolf
Big Bad John
```

* You can get the number of elements of an array 

```bash
$ array=(a b c)
$ echo ${#array [@]}
3
```

### 6.3 - Associative Arrays

* An Associative Array can "map" or "translate" one string to another

```bash
$ declare -A fullNames
$ fullNames=(["lhunath"]="Maarten Billemont" ["greycat"]="Greg Wooledge")
$ echo "Current user is: $USER. Full name: ${fullNames[$USER]}."
Current user is: lhunath. Full name: Maarten Billemont.
```

* You can iterate over the keys of associative arrays

```bash
for user in "${!fullNames[@]}"
do
	echo "User: $user, full name: ${fullNames[$user]}.";
done
```

* The order the keys you get back from an associative array using the `${!array[@]}` 
syntax is unpredictable
* You cannot omit the `$` inside the `[..]`. With indexed array, it's not necessary, but
with associative array, you need it. 

## 7 - Input and Output
### 7.1 - Command-line Arguments

* There are the most used in many scripts : *Positional Parameters*
* You can refer to them by using `$1` `$2` and `${10}`
* You can refer to the entire set of positional parameters with the "$@" substitution
* You can eliminate them by using the command `shift`

### 7.2 - The Environment

* You can access to environment variables in your script
* They have capital letters to avoid accidents
* Use `export MYVAR=something` when you want your changes are only inherited by your 
descendants

### 7.3 - File Descriptors

* File Descriptors are kind of like pointers to sources of data, or places data can be 
written
* *Standard Input* (stdin): File Descriptor 0
* *Standard Output* (stdout): File Descriptor 1
* *Standard Error* (stderr): File Descriptor 2
* It's always a good practice to send your errors to the `stderr` FD like that

```bash
echo "Something went really bad..." >&2
```

### 7.4 - Redirection
#### 7.4.1 - File Redirection

```bash
$ echo "It was a dark night" > story
$ cat story
It was a dark night
```
* The *output redirection* `>` changes the destination of the stdout FD so that it now 
points to a file called `story` 
* Be aware that this redirection occurs before the `echo` command is executed. Bash 
doesn't check to see wheter that file `story` exists first. 
* You should **NOT** use `cat` to pipe files to commands in your script, you should use a 
redirection

* `command > file` : Send the `stdout` of command to `file`
* `command 1> file` : Send the `stdout` of command to `file`. Since `stout` is FD 1, 
that's the number we put in front of the redirection operator. It's identifical to the 
previous example because FD 1 is the default for the `>` operator
* `command < file` : Use the contents of `file` when `command` reads from `stdin`
* `command 0< file` : The same as above

* `stderr` can be used to log error messages

```bash
for homedir in /home/*
do	
	rm "$homedir/secret"
done 2> errors
```

* We redirect errors to the file `errors`. The redirection operator isn't on `rm` but it's
on that `done` thing. Because we want the redirection applies to all output to `stderr` 
made inside the whole loop

* Sometimes you can silence a FD

```bash
for homedir in /home/*
do	
	rm "$homedir/secret"
done 2> /dev/null
```
* Don't forget that `> file` will open `file` and rewrite the content. What if you want 
to append error messages ? Double the redirection operator `>>`

```bash
for homedir in /home/*
do	
	rm "$homedir/secret"
done 2>> errors
```

#### 7.4.2 - File Redirection

* If you want to redirect *stdout* and *stderr* in a file, you can't do it like that : 
`grep proud file 'not a file' > proud.log 2> proud.log` because if you do that, you 
redirect *stdout* to *proud.log* and you redirect *stderr* to the beginning of the file 
*proud.log*

* To do that, you need to use this syntax

```bash
$ grep proud file 'not a file' > proud.log 2>&1
```
First *stdout* is changed so that it points to our *proud.log*. Then we use the syntax to
duplicate FD 1 and put this duplicate in FD 2. 

* Another form of redirection available : The `&>` redirection operator is actually just 
a shorter version of what we did here; redirecting both *stdout* and *stderr* to a file : 

```bash
$ grep proud file 'not a file' &> proud.log
```

## 8 - Ressources
* [Bash Manual](https://www.gnu.org/software/bash/manual/) : Official Bourne-Again SHell manual
* [BashGuide](http://mywiki.wooledge.org/BashGuide) : Bash wiki made by guys from #bash
* [Bash Hackers Wiki](http://wiki.bash-hackers.org/start) : Another Bash wiki
* [ShellCheck](http://www.shellcheck.net/) : Shell script analysis tool
* [Defense bash programming](http://www.kfirlavi.com/blog/2012/11/14/defensive-bash-programming/) : Advices to write modern Bash programs
