package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
	//"strconv"
)

type csvline struct {
	question string
	answer   string
}

func main() {
	csvfilename := flag.String("csv", "problems.csv", "a csv file in the format of 'question,answer'")
	flag.Parse()
	fmt.Println(*csvfilename)
	filename := "problems.csv"
	lines := ReadCSV(filename)

	AskQuestions(lines)
}

func AskQuestions(lines []csvline) {
	totalScore := len(lines)
	score := 0
	reader := bufio.NewReader(os.Stdin)

	for _, line := range lines {
		// ask question
		fmt.Println(line.question)
		// read user input
		answerGiven := ReadUserInput(reader)
		if answerGiven == line.answer {
			score += 1
		}
	}

	fmt.Println("=====================")
	fmt.Println(score)
	fmt.Println(totalScore)
}

func ReadUserInput(reader *bufio.Reader) string {
	answerGiven, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}
	rst := strings.Replace(answerGiven, "\n", "", -1)
	return rst
}

func ReadCSV(filename string) []csvline {
	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	r := csv.NewReader(f)
	lines := make([]csvline, 0)

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		line := csvline{
			question: record[0],
			answer:   record[1],
		}
		lines = append(lines, line)
	}

	return lines
}
