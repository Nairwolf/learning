package main

import (
	"fmt"
	"strconv"
)

func verify(a, b, c int) bool {
	aStr := strconv.Itoa(a)
	bStr := strconv.Itoa(b)
	cStr := strconv.Itoa(c)

	m := make(map[string]int)
	for _, number := range aStr {
		m[string(number)]++
	}

	for _, number := range bStr {
		m[string(number)]++
	}

	for _, number := range cStr {
		m[string(number)]++
	}

	// Verify
	for i := 0; i <= 9; i++ {
		value := m[strconv.Itoa(i)]

		if value != 1 {
			return false
		}
	}

	return true

}

func main() {
	a := 3
	for b := 1; b <= 9999; b++ {
		c := a * b
		rst := verify(a, b, c)
		if rst == true {
			fmt.Printf("%v X %v = %v\n", a, b, c)
			break
		}
	}

	//rst := verify(1, 2345, 67890)
}
