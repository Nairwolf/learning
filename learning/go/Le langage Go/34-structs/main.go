package main

import "fmt"

type Address struct {
	street, city string
}

// Si l'on déclare le nom d'un champ en miniscule, on ne peut pas y accéder depuis
// l'extérieur du package visiblement. Est-ce vrai ?
type Person struct {
	Name string
	Age  int
	Addr Address
}

func main() {
	fmt.Println("vim-go")
	var p Person
	p.Name = "Bob"
	p.Addr.city = "Lyon"
}
