package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type User struct {
	Name  string `json:"user_name"`
	Age   int    `json:"age"`
	Group string `json:"group"`
}

func main() {
	fmt.Println("vim-go")

	u1 := User{Name: "Ichigo", Age: 17, Group: "Shimigami"}

	// On passe d'une structure à un []byte
	res, err := json.Marshal(u1)
	if err != nil {
		panic(err)
	}

	// Conversion de []byte vers string
	json_data := string(res)

	fmt.Println(u1, json_data)

	// On passe d'un json convertit en []byte
	// qui est peuple une structure u2
	u2 := User{}
	err = json.Unmarshal([]byte(json_data), &u2)
	if err != nil {
		log.Fatal(err)
	}

	//fmt.Println(u2)
}
