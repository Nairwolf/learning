package main

import "fmt"

type Address struct {
	City string
}

type User1 struct {
	Name string
	// Un user A une addresse. C'est une relation de possession
	// Addr Address

	// Embedded
	// Address est embarqué dans le type User
	// User EST une addresse
	Address // pas nom de variable !
}

type User struct {
	Name  string
	Email string
}

// Un admin EST un user
type Admin struct {
	User
	Level int
}

type Admin2 struct {
	User  User
	Level int
}

func main() {

	var u User1
	u.City = "Londres" // City est directement accessible

	user := User{
		Name:  "Bob",
		Email: "bob@golang.org",
	}
	fmt.Printf("User: %v\n", user)

	admin := Admin{
		Level: 2,
		User: User{
			Name:  "Alice",
			Email: "alice@golang.org",
		},
	}
	//a.Name = "Alice"
	//a.Email = "alice@golang.org"
	fmt.Printf("Admin: %v\n", admin)

	admin2 := Admin2{
		User:  user,
		Level: 3,
	}
	fmt.Printf("Admin2: %v\n", admin2)

	var adminbis Admin2
	adminbis.Name = "toto"
	adminbis.Email = "toto@golang.org"
	fmt.Printf("adminbis: %v\n", adminbis)
}
