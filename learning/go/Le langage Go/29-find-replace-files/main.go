package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//func FindReplaceFile(src, old, new string) (occ int, lines []int, err error)
// Si la fonction n'arrive pas à ouvrir le fichier, il faut renvoyer une erreur

// func ProcessLine(line, old, new string) (found bool, res string, occ int)

// Lire le fichier line par line
// package bufio
// scanner := bufio.NewScanner(srcFile)
// for scanner.Scan() {
// 	t := scanner.Text()
// }

// Manipulation des strings
// Countains / Count / Replace

// Bonus
// Remplacer le mot en minuscule (gestion de la casse)

// Stocker le résultat dans un fichier à part
// writer := bufio.NewWriter(dstFile)
// defer writer.Flush()
// fmt.Fprintln(writer, "Texte d'une ligne")

func FindReplaceFile(src, dst, old, new string) (occ int, lines []string, err error) {
	file, err := os.Open(src)
	if err != nil {
		return occ, lines, err
	}
	defer file.Close()

	dstFile, err := os.Create(dst)
	if err != nil {
		return occ, lines, err
	}
	defer dstFile.Close()

	writer := bufio.NewWriter(dstFile)
	defer writer.Flush()

	// In order to handle new and old as a complete word
	old = old + " "
	new = new + " "
	nbLine := 1
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		found, res, occLine := ProcessLine(scanner.Text(), old, new)
		occ += occLine
		if found {
			lines = append(lines, strconv.Itoa(nbLine))
		}
		fmt.Fprintf(writer, res)
		nbLine++
	}

	return
}

// ProcessLine searches for old in line to replace it by new
// It returns found=true, if the pattern was found, res with the resulting string
// and occ with the number of occurence of old
func ProcessLine(line, old, new string) (found bool, res string, occ int) {
	found = strings.Contains(line, old)
	occ = strings.Count(line, old)
	res = strings.ReplaceAll(line, old, new)
	return
}

func main() {
	filename := "in.txt"
	output := "out.txt"
	oldWord := "Go"
	newWord := "Python"
	occ, lines, err := FindReplaceFile(filename, output, oldWord, newWord)
	if err != nil {
		fmt.Printf("Error while executing find/replace: %v\n", err)
	}

	fmt.Println("======= Summary =======")
	fmt.Printf("Number of occurences of %v: %v\n", oldWord, occ)
	fmt.Printf("Number of lines: %v\n", len(lines))
	fmt.Println("Lines: [ " + strings.Join(lines, " - ") + " ]")
}
