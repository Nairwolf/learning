package player

type Avatar struct {
	Url string
}

// un champ en minuscule ne sera pas accéssible en dehors du package
type Player struct {
	Name     string
	Age      int
	Avatar   Avatar
	password string
}

func New(name string) Player {
	return Player{
		Name:     name,
		password: "defaultpassword",
	}
}
