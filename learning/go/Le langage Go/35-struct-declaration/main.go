package main

import (
	"fmt"

	"nairwolf.net/go/35-struct-declaration/player"
)

func main() {
	fmt.Println("Il existe différente façon de déclarer une structure")

	// En déclarant la variable d'abord
	var p1 player.Player
	p1.Name = "Bob"
	p1.Age = 10

	fmt.Printf("Player 1: %v\n", p1)
	fmt.Printf("p1.Name=%v, p1.Age=%v\n", p1.Name, p1.Age)

	// c'est possible de déclarer une structure sans nommer les champs
	// mais dans ce cas-là, il faut donner toutes les valeurs
	a := player.Avatar{"https://avatar.jpg"}
	fmt.Printf("Avatar: %v\n", a)

	// Struct Literals
	// Lorsqu'on déclare une structure de cette manière, tout les champs ne sont pas nécessaires.
	p3 := player.Player{
		Name: "John",
		//Age:  25,
		Avatar: player.Avatar{
			Url: "http://example.com",
		},
	}
	fmt.Printf("Player 3: %v\n", p3)

	// Par un constructeur
	// Pour déclarer comment on initialise la structure
	p4 := player.New("Bobette")
	p4.Avatar = a
	fmt.Printf("Player 4: %+v\n", p4)
}
