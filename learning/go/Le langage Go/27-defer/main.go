package main

import (
	"fmt"
)

func start() {
	fmt.Println("Start")
}

func finish() {
	fmt.Println("Finish")
}

func main() {
	start()
	defer finish()

	//defer finish()

	names := []string{"Bob", "Alice", "Bobette", "John"}
	for _, n := range names {
		fmt.Printf("Hello %v\n", n)
		defer fmt.Printf("Goodbye %v\n", n) // LIFO = Last In First Out
	}
}
