package main

import "fmt"

// Rect struct
type Rect struct {
	Width, Height int
}

// Area returns the area of a rect
func (r Rect) Area() int {
	return r.Width * r.Height
}

// DoubleSize the size of Width and Heigh
// But only inside the function
func (r Rect) DoubleSize() {
	r.Width *= 2
	r.Height *= 2
	fmt.Printf("r = %+v\n", r)
}

func (r Rect) String() string {
	return fmt.Sprintf("Rect ==> width=%v, height=%v", r.Width, r.Height)
}

func main() {
	fmt.Println("vim-go")
	r := Rect{2, 4}
	fmt.Println(r)
	fmt.Printf("React area=%v\n", r.Area())

	// Puisqu'il s'agit d'un value receiver
	// r.DoubleSize() ne modifie pas r en dehors de la fonction
	fmt.Println("Double Size")
	r.DoubleSize()
	fmt.Println("r in main()", r)
}
