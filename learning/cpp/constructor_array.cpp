#include <iostream>

class SomethingArray
{
	private:
		const int m_array[5];

	public:
		SomethingArray(): m_array { 1, 2, 3, 4, 5}
		{
		}

		void print_array()
		{
			for (int i=0; i<5; i++)
			{
				std::cout << m_array[i] << std::endl;
			}
		}
};

int main()
{
	SomethingArray array;
	array.print_array();

	return 0;
}
