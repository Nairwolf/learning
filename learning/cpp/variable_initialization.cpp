#include <iostream>

int main()
{
	int value1 = 5; // explicit initialization
	double value2 (4.7); // implicit initialization
	char value3 {'a'}; // uniform initialization C++11 feature

	std::cout << value1 << std::endl;
	std::cout << value2 << std::endl;
	std::cout << value3 << std::endl;

	return 0;
}
