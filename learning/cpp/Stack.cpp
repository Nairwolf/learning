#include <iostream>

class Stack
{
	public:
		void reset() 
		{
			int length = 0;	
			for (int i = 0; i < 10; i++) { 
				array[i] = 0;
			}
		}

		bool push(int a)
		{
			if (length == 10)
				return false;

			array[length++] = a;
			return true;
			}
		}

		int pop()
		{
			if (length == 0)
				return -1;

			// if length=5, need to do `array[4]=0`, so we use pre-incrementation
			return array[--length] == 0; 
			}
		}

		void print()
		{
			int i=0;
			std::cout << "( ";
			for (i; i<length; i++) {
				std::cout << array[i] << " ";
			}
			std::cout << ")" << std::endl;
		}

	private:
		int array[10];
		int length;
};

int main()
{
	Stack stack;
	stack.reset();

	stack.print();

	stack.push(5);
	stack.push(3);
	stack.push(8);
	stack.print();

	stack.pop();
	stack.print();

	stack.pop();
	stack.pop();

	stack.print();

	return 0;
}
