#include <iostream>

template <typename Type>
Type max(Type tX, Type tY)
{
	return (tX > tY) ? tX : tY;
}

int main ()
{
	int a = 2;
	int b = 3;
	int maxint = max(a, b);

	float f1 = 3.14;
	float f2 = 0.00001;
	float f3 = max(f1, f2);

	std::cout << "max integer : ";
	std::cout << maxint << std::endl;

	std::cout << "max float : ";
	std::cout << f3 << std::endl;

	return 0;
}

