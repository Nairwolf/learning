#include <iostream>

class Something
{
	private:
		const int m_value1;
		int m_value2;

	public:
		/* Something() : m_value1(0), m_value2(0)
			{
				// "m_value1 = 0;" doesn't work because "m_value1" is a "const"
			}
		*/
		
		// It's better to use uniform initialization list in C++11
		Something(): m_value1 {0}, m_value2 {1}
		{
		}

		void print_something()
		{
			std::cout << "value1: " << m_value1 << std::endl;
			std::cout << "vlaue2: " << m_value2 << std::endl;
		}
};

int main()
{
	Something smt;
	smt.print_something();
	return 0;
}
