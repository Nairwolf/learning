#include <iostream>

void show_bug(unsigned int x)
{
	std::cout << "x:" << x << std::endl;
}

int main()
{
	// show_bug takes an unsigned int in entry, but the compiler doesn't verify that
	// and converts -1 into 4,294,967,295
	// It's not probably what you wanted
	show_bug(-1);
	return 0;
}
