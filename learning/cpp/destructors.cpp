#include <iostream>

class Simple
{
	private:
		int m_ID;

	public:
			Simple(int ID)
			{
				std::cout << "Constructing Simple " << ID << std::endl;
				m_ID = ID;
			}

			~Simple()
			{
				std::cout << "Destructing Simple" << m_ID << std::endl;
			}

			int getID() { return m_ID; }
};

int main()
{
	Simple simple(1);
	std::cout << simple.getID() << std::endl;

	Simple *pSimple = new Simple(2);
	std::cout << pSimple->getID() << std::endl;
	delete pSimple;

	return 0;
}
