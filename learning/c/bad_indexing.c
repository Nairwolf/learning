#include <stdio.h>

int main(){
	int n = 10;
	int tab[n];
	int a = 0;
	int b = 1;

	printf("addr n = %p\n", &n);
	printf("addr tab[0] = %p\n", &(tab[0]));
	printf("addr tab[8] = %p\n", &(tab[8]));
	printf("addr tab[9] = %p\n", &(tab[9]));
	printf("addr a = %p\n", &a);
	printf("addr b = %p\n", &b);

	for (a = 0; a<20; a++){
		printf("I'm writing to the adress : %p\n", &(tab[a]));
		tab[a] = -1;
	}	   

	printf("b=%d\n", b);

	return 0;
}
