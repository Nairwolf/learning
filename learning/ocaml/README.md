# Ocaml

## Files descriptions

* [hello_world.ml](https://github.com/Nairolf21/learning/blob/master/ocaml/hello_world.ml) 
: Simplest ocaml program
* [typedef.ml](https://github.com/Nairolf21/learning/blob/master/ocaml/typedef.ml) : 
Exercice with Ocaml Type based on this 
[pdf](https://courses.engr.illinois.edu/cs421/sp2012/lectures/lecture3.pdf)
* [bintree.ml](https://github.com/Nairolf21/learning/blob/master/ocaml/bintree.ml) : 
Exercice with binary tree and pattern matching

## Useful ressources

* [Ocaml](http://ocaml.org/) : Official website
* [Programming Languages and Compilers](https://courses.engr.illinois.edu/cs421/sp2012/lectures/) : 
How to build compiler with Ocaml
* [Developing Applications With Ocaml](http://caml.inria.fr/pub/docs/oreilly-book/html/index.html) : 
Freebook about Ocaml programming
* [Real World Ocaml](https://realworldocaml.org/) : Another freebok about Ocaml programming
