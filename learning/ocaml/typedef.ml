type personnalInfo = 
    | Address of int * string
    | Phone of string
    | Age of int 

let get_street info = 
    match info with
      Address (n, addr) -> "address : " ^ addr
    (*   This is not necessery because we use `_` which means `anything` 
       | Phone ph -> "phone : " ^ ph
       | Age a -> "age : " ^ string_of_int a
    *)
    | _ -> "no address known"

let my_street = Address (3, "street of NY")
let phone = Phone ("+33622994400")
let age = Age 17

let () = 
    print_endline (get_street my_street);
    print_endline (get_street phone);
    print_endline (get_street age);
