type bintree = 
    | Empty
    | Node of int * bintree * bintree

let isLeaf btree = 
    match btree with
        | Node (value, Empty, Empty) -> true
        | _ -> false  
    
let rec sum btree = 
    let rval = 0 in 
        match btree with 
            | Node (value, node1, node2) -> rval + value + sum node1 + sum node2
            | _ -> rval

let leaf1 = Node (10, Empty, Empty)
let leaf2 = Node (20, Empty, Empty)
let node_master = Node (5, leaf1, leaf2)

let () = 
    (*print_endline (string_of_bool (isLeaf leaf));*)
    (*  f a b c == (((f a) b ) c ) *)
    print_endline @@ string_of_bool @@ isLeaf leaf1;

    print_endline @@ string_of_int @@ sum node_master;
