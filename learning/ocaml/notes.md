# Installation de l'environnement de développement Ocaml

## Sur une distribution Ubuntu-like

* Installation du paquet `ocaml`
```
sudo apt install ocaml
```

* Installation du paquet `opam` (gestionnaire de paquets)
```
sudo apt install opam
opam init
```
	* Généralement, la dépendance à `m4` est manquante `sudo apt install m4`

*  Configuration de l'environnement
```
eval `opam config env`
```

* Installation de l'interpréteur `utop`
```
opam install utop
```

* Lancement de l'interpréteur
```
utop
```
