# Notes à propos d'ELK

## Concepts
* Cluster : Un cluster est une collection d'un ou plusieurs noeuds collectant l'ensemble des données
* Node : Un node est un serveur collectant les données. Il est identifié par un UUID.
* Index : Collections de documents avec des caractéristiques similaires. On peut définir autant 
d'index que l'on souhaite.
* Document : Il s'agit d'une donnée que l'on souhaite indexé. Un 'document' est exprimé en JSON.
* Shards : Un index possédant un milliards de documents, occupant une place de 1TB pourrait dépasser
les capacités hardware d'un serveur. Pour résoudre cela, on peut découper un index en plusieurs 
parties que l'on appelle 'shards'. Cela permet de faire de la réplication, et de la parallélisation.

## API Rest
Elasticsearch possède une API Rest pour explorer les données et intéragir avec.

* Santé du cluster : `curl -X GET "localhost:9200/_cat/health?v"`
* Lister les noeuds d'un cluster : `curl -X GET "localhost:9200/_cat/nodes?v"`
* Lister ses index : `curl -X GET "localhost:9200/_cat/indices?v"`

On peut aussi effectuer des recherches sur ses données : 
`curl -X GET 'http://127.0.0.1:9200/<index_name>/_search?q=field1&sort=field2:asc&pretty`

* `q=field1` : Effectue la recherche sur le champ `field1`
* `sort=field2:asc` : Tri croissant sur `field2`
* `pretty` : Affiche de manière lisible les résultats

Autre manière d'effectuer la recherche avec une requête utilisant le 
[Query DSL](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html)
```
curl -X GET "localhost:9200/bank/_search" -H 'Content-Type: application/json' -d'
{
  "query": { "match_all": {} },
  "sort": [
    { "account_number": "asc" }
  ]
}
'
```


## Ressources
* [Documentation officielle](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html)
