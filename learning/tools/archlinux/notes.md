# Notes

## Disposition du clavier:
* `loadkeys fr-pc`

## Réseau
* Utilisation du script `wifi-menu` présent par défaut

## Heure
* Pas de problème : `timedatectl`

## Partitionnement des disques
* Utilisation de cfdisk
    * /dev/sda1 : 500 Mo EFI System
    * /dev/sda2 : 50 Go Linux Filesystem
    * /dev/sda2 : 70 Go Linux Filesystem (sera utilisé pour Docker)
    * /dev/sdb1 : 119 Go Linux Filesystem

* Formatage des partitions:
```bash
mkfs.vfat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sda3
mkfs.ext4 /dev/sdb1
```

* Montage des partitions
```bash
mount /dev/sda2 /mnt
mkdir /mnt/home && mount /dev/sdb1 /mnt/home
mkdir /mnt/boot/efi && mount -t vfat /dev/sda1 /mnt/boot/efi
```

## Installation du système de base
Pour choisir un mirroir plus proche, on peut utiliser l'utilitaire `rankmirror`, il faut l'installer 
avec : 

* `pacman -Sy pacman-contrib` 

Ensuite, on peut choisir son mirroir en faisant: 
```
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
sed -s 's/^#Server/Server/' /etc/pacman.d/mirrorlist.backup
rankmirrors -n 10 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist
```
* Paquets intéressant à installer : pkgfile
* Pour la configuration réseau : wireless_tools, wpa_supplicant, dialog

## Configuration du système
Après avoir _chrooté_ dans le nouveau système : 
* `ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime`
* Décommenter la locale `fr_FR.UTF-8 UTF-8`

* Après avoir fait `mkinitcpio -p linux`, j'obtiens deux warning : 
```
Possibly missing firmware for module: aic94xx
Possibly missing firmware for module: wd719x
```

# Installation du Bootloader Grub
* `pacman -S grub efibootmgr`
* `mkdir -p /boot/efi/EFI`
* `grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=arch_grub --recheck`
* `grub-mkconfig -o /boot/grub/grub.cfg`

# Pacman 
* [Doc officielle](https://wiki.archlinux.fr/Pacman)

# Utilisateurs
```
useradd -G wheel -m nairwolf
passwd nairwolf
```

# Paquets installés
```
base wireless_tools wpa_supplicant dialog grub efibootmgr vim net-tools xorg-server xorg-xinit
lightdm lightdm-gtk-greeter xfce4 xfce4-goodies
```

# Ressources
* [Guide officiel d'installation francophone](https://wiki.archlinux.fr/Installation)
* [Guide officiel d'installation anglophone](https://wiki.archlinux.org/index.php/Installation_guide)
