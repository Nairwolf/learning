# Notes à propos de Docker

## Installer Docker
* [Installer Docker-engine sur Linux](https://docs.docker.com/engine/getstarted/linux_install_help/)
* [Installer docker-compose (utilisation de pip)](https://docs.docker.com/compose/install/) 

Après installation, ajouter son username dans le groupe 'docker' : `sudo usermod -aG docker <username>`.
Il faut ensuite se déconnecter et se reconnecter pour que cela prenne effet.


## Gestion des images

* Lister les images téléchargées localement

```bash
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
hello-world         latest              693bce725149        6 days ago          967 B
postgres            9.5                 0f3af79d8673        10 weeks ago        265.7 MB
postgres            latest              0f3af79d8673        10 weeks ago        265.7 MB
```

* Supprimer des images
`docker rmi <image name>` ou par leur ID : `docker rmi 693`. 
Il est possible de supprimer des images, même si celles-ci sont utilisés par un containeur. 
Elles sont tout simplements "détaggués". 

    * Supprimer toutes les images sans containeurs lancés : 
        `docker rmi $(docker images -qa)`
    * Supprimer toutes les images :
        `docker rmi -f $(docker images -qa)`
    * Supprimer toutes les images suspendues : 
        `docker images -q --no-trunc -f dangling=true | xargs -r docker rmi`

## Gestion des conteneurs

* Lister les conteneurs en route : `docker ps`
* Lister tout les conteneurs : `docker ps -a`
* Supprimer un conteneur : `docker rm <container-ID>` 
* Supprimer tout les conteneurs : `docker rm $(docker ps -aq)`

## Libérer de l'espace mémoire

* Libére tout les volumes non-utilisés : `docker volume prune`

## Ressources
### Documentation officielle : 
* [Tutoriel basique](https://docs.docker.com/engine/getstarted/#got-docker)
* [Docker CLI reference](https://docs.docker.com/engine/reference/commandline/cli/)
* [Dockerfile reference](https://docs.docker.com/engine/reference/builder/)
* [Docker-compose reference](https://docs.docker.com/compose/compose-file/)
