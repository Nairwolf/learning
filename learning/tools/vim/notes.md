# Notes à propos de Vim

## Raccourcis claviers (peu connus)

### Se déplacer

* `{` et `}` : Aller au paragraphe précédent / suivant 
* `'.` : Aller à la position de la dernière édition
* `''` : Aller au dernier point avant un saut
* `^F / ^B` : Avancer / Reculer d'une page
* `^E / ^Y` : Avancer / Reculer ligne par ligne

### Éditer

* `.` : Répeter la dernière commande
* `s` : Remplace un seul caractère et reste en mode édition
* `xp` : Intervertit deux caractères (`x` supprime et `p` copie après le curseur)

### Sélection

* `v` / `V` / `^V` : sélection en ligne / blocs / colonnes
* `o` : déplace le curseur de l'autre côté du bloc
* `gv` : réselectionne le dernier bloc

### Fichiers

* `ZZ` : écris le fichier et quitte
* `:n <filename>` : édite un nouveau fichier
* `:e.` : Affiche l'explorateur de fichiers

### Déplacement fenêtres

* `^Wn` : Créer une nouvelle fenêtre
* `^Wj` / `^Wk` : Se déplacer dans les fenêtres
* `^W_` / `^W=` : Maximise / Rend égales les tailles de fenêtre

### Se déplacer dans un fichier source

* `%` : Se déplacer vers la parenthèse / accollade correspondance (ou blocs si un module du langage est activé)
* `gd` / `^O` : Se déplacer vers la définition de l'objet ou méthode / Revient à la position précédente
* `^N` : Autocomplétion

## Astuces

### Astuces sur la recherche de mots ou expressions :

* Faire une recherche sur un mot exact : `/\<exact_word\>`
* Lorsque le curseur est sur un mot : `*` cherche les occurences du prochain mot, et `#` en arrière
* Si l'on ne cherche pas le mot exact : `g*` ou `g#`

### Ignorer le case-sensitive : 

* `:set ignorecase`
* On peut utiliser `\c` pour forcer le case-sentive, ou `\C` pour ne pas l'être.
* Si on rajoute, `:set smartcase`, la recherche sera case-sensitive si le pattern a une majuscule.

### Rechercher un pattern après avoir sélectionner un texte : 

1. Sélectionner un texte `v`
2. Le copier `y`
3. Taper `/` pour lancer la recherche
4. Insérer le texte dans un registre `<C-R>"`
5. Valider avec ENTREÉ
6. Rechercher les éléments avec `n` ou `N`

* [https://vimhelp.com](https://vimhelp.appspot.com/cmdline.txt.html#c_CTRL-R) et 
* [http://of-vim-and-vigor.blogspot.fr](http://of-vim-and-vigor.blogspot.fr/2012/01/rap-on-vims-registers.html)

=> Ajout de l'alias **//** dans .vimrc :
* `vnoremap // y/<C-R>"<CR>` : Voir explication plus haut

### Tour de sorcellerie

Incrémenter cette ligne : 
`6. e4 e5 7. Nf3 Nc6 8. Bc4 Bc5`

* Recherche des coups : `/\d\+\.`
* Enregistrement d'une macro : qa
* Incrémentation de 5 et passage à l'élément suivant : 5 <C-a>n
* Fin de la macro : q
* Excécution de la macro : @a
* Répétition de la dernière macro : @@

Bonus : 
* Répéter 10 fois la macro : 10@a
* Empêcher la macro de se répéter : set nowrapscan

### Commenter plusieurs lignes

* CTRL + V : Visual Block
* Shift + I : Insert mode
* Taper '#'
* Echap

### Modifier l'encoding d'un fichier texte

Certains fichiers sont sensibles à l'encoding (code python), un fichier écrit depuis une 
plateforme Windows aura des difficultés à s'exécuter sur Linux. La commande suivante
permet de corriger l'encodage. 

```
:se ff=unix
```

### Mettre en majuscule ou en minuscule un mot

* gcw        - capitalize word (from cursor position to end of word)
* gcW        - capitalize WORD (from cursor position to end of WORD)
* gciw       - capitalize inner word (from start to end)
* gciW       - capitalize inner WORD (from start to end)
* gcis       - capitalize inner sentence
* gc$        - capitalize until end of line (from cursor postition)
* gcgc       - capitalize whole line (from start to end)
* gcc        - capitalize whole line
* {Visual}gc - capitalize highlighted text

### Search and Replace

* [Vim Fandom](https://vim.fandom.com/wiki/Search_and_replace)
* [Regular Expressions](https://learnbyexample.gitbooks.io/vim-reference/content/Regular_Expressions.html)
* [Vimregex](http://www.vimregex.com/)

Pour substituer un texte sur une sélection visuelle faire:

1. Sélectioner un texte
2. `:s` va rajouter le range `:'<,'>`
3. La commande finale sera donc : `:'<,'>s/old/new/g`

Cette commande s'appliquer sur toutes les lignes sélectionnées, mais dans le cas d'une
sélection par bloc, si l'on souhaite strictement la zone sélectionner faire : 

`:s/\%Vold/new/g`

### Plugins

Depuis vim8, les plugins sont gérés automatiquement par Vim.

1. Ajouter `packloadall` sur `~/.vimrc`
2. Créer le plugin directory: `mkdir -p ~/.vim/pack/plugins/start`
3. Cloner le plugin: `git clone -b 'v1.2' --depth=1 https://example.com/example.git
~/.vim/pack/plugins/start/example`
4. Pour mettre à jour: `ls ~/.vim/pack/plugins/start | xargs -I{} git pull {}`
5. Pour générer la doc des plugins faire: `:helptags ALL`

# Ressources :

* [Vim Wikia](http://vim.wikia.com/wiki/Capitalize_words_and_regions_easily)
* [Vim and Python - A Match Made in Heaven](https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/)
* [A Good vimrc](https://dougblack.io/words/a-good-vimrc.html)

# Liste de plugins intéressants

* [Vim-commentary](https://github.com/tpope/vim-commentary)
