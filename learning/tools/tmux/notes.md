# Notes sur Tmux

# Gestion des sessions
* `prefix + d` : Détache la session en cours
* `prefix + D` : Détache la session sélectionnée
* `tmux ls` : Affiche l'ensemble des sessions
* `tmux new -s <name-session>` : Créer une session avec un nom
* `tmux attach -t 0` : Attache la session 0
* `tmux swtich -t session_name` : Change de session
* `tmux rename-session -t 0 <name-session>` : Renomme une session

# Fenêtres
* `prefix + c` : Création d'une nouvelle fenêtre
* `prefix + n` / `prefix + p` : Déplacement dans les fenêtres
* `prefix + <number>` : Accéder à la fenêtre <number>
* `prefix + ,` : Renomme une fenêtre
* `prefix + .` : Déplace l'index d'une fenêtre (tmux movew -s <from-window> -t <to-window>)
* `prefix + <space>` : Change de disposition des fenêtres (layout)

# Panels
* `prefix + %` : Split vertical
* `prefix + "` : Split horizontal
* `prefix + <arrow key>` : Déplacement de panel
* `Ctrl-d` : Fermer un panel
* `prefix + { or }` : Échange un panel

# Autres commandes
* `prefix + z` : Affiche un panel en grand écran
* `prefix + C-<arrow-key>` : Redimensionne un panel
* `tmux list-keys` : Affiche l'aide
* `tmux list-commands`: Affiche les commandes tmux
* `tmux info` : Affiche toutes les infos
* `tmux source-file ~/.tmux.conf` : Recharge la configuration tmux

# Ne fonctionne pas...
* Clipboard copy and paste
* `set -g base-index 1` : Pour décaler l'index d'une fenêtre



# Ressources
* [A Quick and Easy Guide to tmux](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/)
* [A tmux Crash Course](https://robots.thoughtbot.com/a-tmux-crash-course)
* [The Tao of tmux](https://leanpub.com/the-tao-of-tmux/read)
