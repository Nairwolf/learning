# XFCE
Notes à propos de XFCE

## Remettre à zéro la configuration XFCE

```bash
sudo xfce4-panel --quit # quitter l'interface XFCE
sudo pkill xfconfd # forcer l'extinction des processus de configuration de XFCE
sudo rm -rf ~/.config/xfce4 # supprime tout la configuration XFCE
```

On supprime tout, et en redémarrant une session, on revient à la configuration par défaut
