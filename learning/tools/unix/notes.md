# Sommaire
1. [Outils Unix](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#1-outils-unix)
	1. [Grub](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#11-grub)
	2. [apt](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#12-apt)
	3. [Outils de création et d'édition de PDF](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#13-outils-de-cr%C3%A9ation-et-d%C3%A9dition-de-pdf)
2. [Commandes Unix](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#2-liste-de-commandes-unix-utiles)
	1. [Back-up incrémentales avec rsync](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#21-faire-des-back-up-incr%C3%A9mentales-avec-rsync)
	2. [Excécuter une commande sur un ensemble de fichiers](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#22-ex%C3%A9cuter-une-commande-sur-un-ensemble-de-fichiers)
	3. [Paramètrer par défaut les fichiers et répertoires](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#23-param%C3%A8trer-les-droits-par-d%C3%A9fauts-pour-les-fichiers-et-pour-les-r%C3%A9pertoires)
	4. [Créer une clé usb bootable](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#24-cr%C3%A9er-une-cl%C3%A9-usb-bootable)
	5. [Obtenir des infos spécifiques sur sa distribution](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#25-obtenir-des-infos-sp%C3%A9cifiques-sur-sa-distribution)
	6. [Identifier les processus utilisant un système de fichier](https://github.com/Nairwolf/learning/blob/master/tools/unix/notes.md#26-identifier-les-processus-utilisant-un-syst%C3%A8me-de-fichiers)

# 1. Outils Unix

## 1.1 Grub
Grub est un bootloader, c'est le premier logiciel qui s'exécute quand un ordinateur 
démarre. C'est lui qui donne le contrôle au noyau du système d'exploitation. En cas de 
dual-boot, c'est le grub du dernier OS installé qui est utilisé.

Grub utilise le fichier `/boot/grub/grub.cfg`, il est généré automatiquement par la 
commande `update-grub`. Pour configurer le grub, il faut modifier le fichier 
`/etc/default/grub`. Sont aussi utilisés les fichiers du dossier `/etc/grub.d/`.

### Options de configuration Grub
* GRUB_DEFAULT=0 : Corresond au menu qui sera sélectionné par défaut
* GRUB_HIDDEN_TIMOUT=0 : Le nombre fixe en secondes, la durée permettant d'appuyer sur les touches **Échap** ou **Shift** pour faire apparaître le menu
* GRUB_HIDDEN_TIMEOUT_QUIT=false : Affichage ou non du décompte pour afficher le menu
* GRUB_TIMEOUT=10 : Durée en secondes de l'affichage du menu. Si la valeur vaut **-1**, le
menu restera présent tant que l'utilisateur n'aura pas valider de choix
* GRUB_DISTRIBUTOR="" : Ligne qui définit la syntaxe des titres du menu

Lorsque les options de configuration Grub sont modifiés, ne pas oublier de mettre à jour 
Grub avec la commande `sudo update-grub`.

## 1.2 apt
`apt` est le gestionnaire de paquet par défaut dans le monde Debian/Ubuntu. Il s'agit 
d'une surcouche à `apt-get` qui est lui aussi une surcouche à des outils comme `dpkg`. 
`apt` regroupe aussi les fonctionnalitées de `apt-cache`.

* `update` : Télécharge les informations sur les paquets en provenance des dépôts 
configurés. Permet de faire la mise à jour des paquets installés ou de lister les 
paquets disponibles à l'installation.
* `upgrade` : Installe les mises à jour disponibles de tout les paquets installés. 
S'il y a besoin de satisfaire des dépendances, de nouveaux paquets seront installés, mais 
ceux existant ne seront jamais effacé. Si la mise à jour d'un paquet nécessite la 
suppression d'un paquet installé, la mise à jour de ce paquet n'est pas effectué.
* `full-upgrade` : Installe les mises à jour disponibles, et supprimer des paquets
si cela est nécessaire.
* `install, remove, purge` : Effectue l'opération demandé sur un paquet. `remove` permet
la suppression du paquet, mais conserve les fichiers de configuration. 
* `autoremove` : Supprime les paquets qui ont été installés automatiquement pour 
satisfaire les dépendences d'autres paquets, et qui ne sont plus nécessaires. Si l'on 
souhaite conserver un paquet, on peut le marquer comme étant manuellement installé via
la commande `apt-mark`. 
* `search` : Effectue une recherche sur les paquets disponibles. Les regex sont 
disponibles.
* `show` : Affiche les informations sur un paquet (dépendences, taille de téléchargement 
et d'installation)
* `list` : Affiche une liste de paquets satisfaisant certains critères comme `--installed`
pour les paquets installés, `--upgradable` pour les paquets à mettre à jour, ou tout les
paquets disponibles avec `--all-versions`.
* `edit-sources` : Permet d'éditer le fichier `/etc/apt/sources.list` avec l'éditeur par
défaut.

## 1.3 Outils de création et d'édition de PDF

* `pdftk` : Outil de manipulation générale sur les PDFs 
	* Assemblage de deux PDFs `pdftk recto.pdf verso.pdf cat final.pdf`
* `pdfseparate` : Sépare et extraits différentes pages d'un PDF
* `pdfimages` : Convertit un PDF en un fichier image (JPEG, PNG, etc)
* `convert` : Outil de traitement d'image, et permet d'assembler plusieurs images en un seul pdf
	* Assemblage de plusieurs images en un pdf `convert *.jpg foo.pdf`
    * Pour redimensionner un ensemble fichiers .jpg : `for file in *.jpg; do convert "$file" -resize 50% $file"; done`

# 2. Liste de commandes Unix utiles

## 2.1 Faire des back-up incrémentales avec rsync

* `rsync /source destination/` ou `rsync -az /source login@server.org:/destination/` via SSH
* `rsync -a --stats --progress --delete /source /destination/` : Utilise rsync en mode *archive*, `--stats --progress` affichent des infos durant le transfert, `--delete` efface avant le transfert des fichiers ceux qui n'existent pas sur la source.

### Options utiles

* `-v` : mode verbeux
* `-z` : compresse les fichiers
* `--delete-after` : à la fin du transfert, supprime les fichiers dans le dossier destionation non présents dans le dossier source
* `-n` : liste ce que la commande va faire sans l'exécuter
* `--exclude="nom_dossier"` ou `--exclude-from=ExclusionRsync` : Pour spécifier les dossiers et fichiers à exclure.

## 2.2 Exécuter une commande sur un ensemble de fichiers

On souhaite par exemple exécuter la commande `ln filename.txt /mon_repertoire/filename.txt` sur un ensemble de fichiers. 

### Méthode avec boucle for :

```bash 
$ for i in *.txt; do ln %i /chemin/%i; done
```

### Méthode avec find :

```bash
$ find -type f -name "*.txt" -exec ln {} chemin/ \;
```

## 2.3 Paramètrer les droits par défauts pour les fichiers et pour les répertoires

### Fichiers : 

```bash
find /dir -type f -exec chmod 644 '{}' \;
```

### Répertoires : 

```bash
find /dir -type d -exec chmod 755 '{}' \;
```

Attention au dossier `.ssh` qui doit avoir des droits spéciaux :
* `chmod go-w ~/`
* `chmod 700 ~/.ssh`
* `chmod 600 ~/.ssh/authorized_keys` 

## 2.4 Créer une clé usb bootable

1. Identifier l'emplacement de la clé usb monté : `lsblk`
2. Démonter la clé usb : `umount /media/user/mountpoint`
3. Utiliser dd pour écrire l'image : `sudo dd if=/path/to/image.iso of=/dev/sdX bs=1M status=progress && sync`
4. La clé usb peut avoir besoin d'être re-formaté en fat32 : `sudo mkfs.msdos -F 32 /dev/sdX` 

## 2.5 Obtenir des infos spécifiques sur sa distribution

* `cat /var/log/installer/media-info` ou `cat /var/log/installer/lsb-release`
* `lsb_release -a`
* `echo $XDG_CURRENT_DESKTOP` : Pour obtenir l'environnement graphique
* `echo $GDMSESSION`
* `uname -a` : Version du noyau utilisé
* Le package `screenfetch` permet d'afficher différentes info sur le système utilisé

## 2.6 Identifier les processus utilisant un système de fichiers

Lorsqu'on souhaite démonter un disque, mais qu'un processus utilise ce disque, il peut
être intéressant d'exécuter cette commande pour obtenir le PID du processus qui 
utilise le disque. 

```bash
fuser -m /media/nairwolf/external_drive
```

## 2.7 Remplacer un texte sur plusieurs fichiers en même temps

```
grep -r -l <old> . | xargs sed -i 's/<old>/<new>/g'
```

## 2.8 Utilisation de xargs pour déplacer plusieurs fichiers

On peut utiliser l'utilitaire `xargs` pour récupérer des items depuis la sortie standard. Le cas 
le plus fréquent est celui-ci : 

```
# suppression de tout les fichiers 'core' dans '/tmp'
find /tmp -name core -type f -print | xargs /bin/rm -f
```

La commande `xargs` a cette signature : xargs [options] [command [initial_arguments]].
Que faire lorsqu'on veut passer plusieurs paramètres à `command` ? 

On peut utiliser l'option -I: 

```
find . -type f -path *.pdf | xargs -I '{}' mv '{}' my_dest/
```

## 2.9 Éditer du texte depuis STDIN vers STDOUT

```
cat file.txt | sed -e 's/foo/bar/g'
```

L'option `-e` permet de faire cela : https://stackoverflow.com/q/9984748

## 2.10 Nettoyer les fichiers de configurations des paquets supprimés

Lorsqu'on fait `dpkg -l` on peut trouver qu'il y a certains paquets supprimés, mais avec
les fichiers de configuration toujours présents.

Pour faire le nettoyage, il faut faire:

```
dpkg --list | grep "^rc" | cut -d " " -f 3 | xargs sudo dpkg --purge
```

Pour connaître le contenu des fichiers de configuration:

```
apt-file list <package_name>
```

## 2.11 Nettoyer anciens noyaux

```
uname -r
dpkg --list 'linux-image*' | grep ^ii
sudo apt-get remove linux-image-VERSION
```
