# Using syslog-ng with Docker

* [Your central log server in docker](https://syslog-ng.com/blog/central-log-server-docker/)
* [Collecting Docker infrastructure logs using syslog-ng](https://syslog-ng.com/blog/collecting-docker-infrastructure-logs-using-syslog-ng/)
* [Collecting logs from containers using Docker voluems](https://syslog-ng.com/blog/collecting-logs-containers-using-docker-volumes/)
