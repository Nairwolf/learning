# Notes à propos de Git

## Trouver l'ancêtre commun entre deux branches
* `git merge-base branch1 branch2`

## Récupèrer un fichier supprimé
* Afficher les ficheirs supprimés dans le log : `git log --diff-filter=D --summary`
* Trouver le dernier commit qui a affecté un chemin : `git rev-list -n 1 HEAD -- <path>`
* On peut faire un checkout du fichier : `git checkout <rev-number>^ -- <path>`
* On peut effectuer cette action durant un rebase interactif

## Suppression d'une branche:

* Branche locale : `git branch -d local_branch`

* Branche distante : `git push origin :remote_branch`

## Nettoyage d'un repo Git:

* `git clean -dfx`

## Récupérer la version d'un fichier présent sur une autre branche
* `git checkout <branch-name> -- <filepath>` : 
See [explanation](http://nicolasgallagher.com/git-checkout-specific-files-from-another-branch/)

## Ressources
* [Site officiel](https://git-scm.com/)
* [A successfull Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)
* [Learn enough Git to be dangerous](https://www.learnenough.com/git-tutorial)
* [Git ref](https://gitref.org)
* [Git tips](https://github.com/git-tips/tips)
* [Git ready](http://gitready.com/)
* [Git book](https://git-scm.com/book/en/v2)
* [Git workflow](http://documentup.com/skwp/git-workflows-book)
* [Choose your adventure guide on undoing your git fuckup](https://sethrobertson.github.io/GitFixUm/fixup.html)
