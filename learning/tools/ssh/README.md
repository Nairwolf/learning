# SSH

## Se déconnecter d'une connection SSH qui ne réponds pas :
* Taper : `<enter>~.`

## Utiliser sa clef ssh lorsqu'on se connecte sur un serveur 
* ssh -A <name>@<serveur>

## Authentification par un système de clés publique/privée

* Création d'une paire de clé : 

À enregistrer dans *~/.ssh*:
`ssh-keygen -t rsa`

* Envoyer la clé publique sur le serveur : 
```bash
ssh-copy-id -i ~/.ssh/id_rsa.pub <username>@<ipaddress>
```

## Gestion des droits SSH
* Client : 

```bash
chmod 755 $HOME`
```

* Server : 

```bash
chmod go-w ~/
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
```

## Notes

Pour vérifier le stock d'entropie sur la machine :
`cat /proc/sys/kernel/random/entropy_avail`

Après l'installation d'un démon pour récolter de l'entropie : 
`apt install haveged`, le stock d'entropie devrait être plus élevé
