# Notes sur Python

## Sommaire
1. [Apprentissage](https://github.com/Nairwolf/learning/blob/master/python/notes.md#1-apprentissage)
	1. [Les trucmuchables en Python](https://github.com/Nairwolf/learning/blob/master/python/notes.md#11-les-trucmuchables-en-python)
2. [Techniques de programmation](https://github.com/Nairwolf/learning/blob/master/python/notes.md#2-techniques-de-programmation)
	1. [Gestion de l'Unicode](https://github.com/Nairwolf/learning/blob/master/python/notes.md#21-gestion-de-lunicode)
3. [Installation et configuration](https://github.com/Nairwolf/learning/blob/master/python/notes.md#3-installation-et-configuration)
	1. [pip - gestionnaire de dépendances Python](https://github.com/Nairwolf/learning/blob/master/python/notes.md#31-pip---gestionnaire-de-d%C3%A9pendances-python)
	2. [virtualenv](https://github.com/Nairwolf/learning/blob/master/python/notes.md#32-virtualenv)
4. [Documentation officielle](https://github.com/Nairwolf/learning/blob/master/python/notes.md#4-documentation-officielle)
5. [Ressources](https://github.com/Nairwolf/learning/blob/master/python/notes.md#5-ressources)

## 1. Apprentissage

### 1.1 Les trucmuchables en Python

#### 1.1.1 Iterable

Un iterable est un objet qui accepte l'itération (usage de la boucle `for`). On peut 
*itérer* sur des :

**Listes** :
```python
for x in ['une', 'liste']:
	print(x)
une
liste
```

**Chaîne de caractères** :
```python
for x in 'unechaine':
	print(x)
u
n
e
c
h
a
i
n
e
```

**Tuples** :
```python
for x in ('un', 'tuple'):
	print(x)
un
tuple
```

**Dictionnaire** :
```python
for x in {'un':'dico', 'par':'cle'}:
	print(x)
un
par
```

**Sets** : 
```python
for x in set(('un', 'set')):
	print(x)
un
set
```

**Fichiers** :
```python
with open('/tmp/test', 'w') as f:
	f.write('un\nfichier')

for x in open('/tmp/test'):
	print(x, end="")
un
fichier
```

Beaucoup de structures de données du module `collections` sont itérables (`deque`, 
`namedtupple`, `defaultdict`). 

Les générateurs sont itérables :
```python
def generator():
	yield 1
	yield 2

for x in generator():
	print(x)
1
2
```

Pour vérifier si un objet est itérable, on peut utiliser la fonction `iter()`. Cette 
fonction prend un itérable et retourne un générateur qui permet d'énumérer chaque élément 
de l'itérable via la fonction `next()`. Il s'agit du procédé utilisé dans une boucle 
`for`. La fonction `iter()` lève `TypeError` sur un non iterable. On peut définir son 
propre objet itérable en définissant la méthode `__iter__` qui doit retourner un 
générateur : 

```python
class NouvelIterable:
	def __iter__(self):
		# mettre des yield marche aussi
		return iter([1, 2, 3])

for x in NouvelIterable():
	print(x)
1
2
3
```
**Source** :
* [Les trucmuchables en Python](http://sametmax.com/les-trucmuchables-en-python/)

## 2. Techniques de programmation

### 2.1 Gestion de l'Unicode

Pour transformer des caractères spéciaux en ASCII :
* [Sametmax](http://sametmax.com/transformer-des-caracteres-speciaux-en-ascii/)
* [Code sample](https://gist.github.com/Nairwolf/473fd561af96b196b591885522becd24)
* [Code sample plus court](https://gist.github.com/Nairwolf/05f7b5c952eb70daafa0e92d8d3f0845)
* Oneliner : 
```python
''.join(c for c in unicodedata.normalize('NFKD', 'é') if not unicodedata.combining(c)) 
# return 'e'
```

Documentation sur l'Unicode :
* [Unicodebook](https://unicodebook.readthedocs.io/programming_languages.html#python)


### 2.2 GUI Application
Pour utiliser Tkinter, il est nécessaire d'installer le package `python3-tk`

### 2.3 MySQL Python
* [Documentation officielle](http://mysqlclient.readthedocs.io/index.html)
* [Tutoriel](http://zetcode.com/db/mysqlpython/)

## 3. Installation et configuration

### 3.1 pip - gestionnaire de dépendances Python

`pip` est le gestionnaire de paquets de Python pour importer des libraires depuis 
[PyPI](https://pypi.python.org/pypi).
Sur Ubuntu, il peut s'installer via les paquet `python-pip` (pour Python2) et `python3-pip`.

Il est recommandé de ne pas lancer `pip` en mode root pour ne pas installer des paquets 
qui ne seraient pas pris en compte par `apt`. 

**Source** :
* [Votre Python aime les pip](sametmax.com/votre-python-aime-les-pip)
* [What are the risks of running sudo pip ?](https://stackoverflow.com/questions/21055859/what-are-the-risks-of-running-sudo-pip)

### 3.2 virtualenv

`virtualenv` est un gestionnaire d'environnement virtuel. Il permet d'isoler 
l'installation de libraire de manière indépendate à un projet donné, pour éviter 
d'éventuels conflits. Pour installer `virtualenv`, on fera : 

```
pip install --user virtualenv
```

Le binaire se trouve ensuite dans `~/.local/bin/`. Ajout le chemin dans le `$PATH` est 
une bonne idée. 

Ensuite, on peut se créer un dossier `~/.virtualenvs/` où l'on va stocker les différents 
environnements pour les projets. 

```
$ cd ~/.virtualenvs
$ virtualenv env-name # Installation de tout un tas de librairies
$ source ~/.virtualenvs/env-name/bin/activate # activation de l'environnement virtuel
```

Une fois l'environnement virtuel crée, toutes les librairies installées via `pip` seront
spécifiques à cet environnemnet. 

Le wrapper `virtualenvwrapper` peut-être utilisé pour facilier l'utilisation de 
`virtualenv`

**Source** :

* [Les environnements virtuels Python : virtualenv et virtualenvwrapper](http://sametmax.com/les-environnement-virtuels-python-virtualenv-et-virtualenvwrapper/)
* [Virtualenv : documentation officielle](https://virtualenv.pypa.io/en/stable/)
* [Virtualenvwrapper : documentation officielle](https://virtualenv.pypa.io/en/stable/)
* [Pew mieux que virtualenvwrapper](http://sametmax.com/mieux-que-python-virtualenvwrapper-pew/)

## 4. Documentation officielle

* [Tutoriel officiel](https://docs.python.org/3/tutorial/index.html)
* [Python Standard Library](https://docs.python.org/3/library/index.html)

## 5. Ressources

* [Sam et max](sametmax.com) : Attention NSFW
* [Full Stack Python](http://www.fullstackpython.com/)
* [The HitchHiker's Guide to Python](http://docs.python-guide.org/en/latest/index.html)
* [Think Python](http://greenteapress.com/wp/think-python-2e/)
* [The Python Way: 10 Tips](http://raymondtaught.me/the-python-way-10-tips/)
* [/r/pythontips](https://www.reddit.com/r/pythontips/)
* [Python Import](https://chrisyeh96.github.io/2017/08/08/definitive-guide-python-imports.html#example-directory-structure)
