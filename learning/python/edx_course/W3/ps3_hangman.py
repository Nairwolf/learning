#!/usr/bin/env python3
#
# Hangman game
#

# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)


import random
import string
import unicodedata

def loadWords():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    lang = None
    while lang not in ('f', 'e'):
        lang = input("Choose your language: [f]rench or [e]nglish : ")

    wordlist_filename = 'words.txt' if lang == 'e' else 'mots.txt' 

    print("Loading word list from file...")

    # inFile: file
    inFile = open(wordlist_filename, 'r')
    worddict = {} # dict {'unicode':'ascii'}
    for line in inFile:
        word_unicode = line.strip().lower()
        word_ascii = unicode2ascii(word_unicode)
        worddict[word_unicode] = word_ascii

    print("  ", len(worddict), "words loaded.")
    return worddict

def unicode2ascii(word):
    """
    Converts a word encoded with Unicode to ascii
    unicode2ascii('éàïêù') = 'eaieu'
    """
    return ''.join(c for c in unicodedata.normalize('NFKD', word) if not unicodedata.combining(c))

def chooseWord(worddict):
    """
    worddict (dict): dict of words {string:string}

    Returns the tuple (word_unicode, word_ascii) from worddict at random
    """
    w_unicode = random.choice(tuple(worddict.keys()))
    w_ascii = worddict[w_unicode]
    return w_unicode, w_ascii

# end of helper code
# -----------------------------------

def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    for letter in secretWord:
        if letter in lettersGuessed:
            rst = True
        else:
            rst = False
            break
    return rst




def getGuessedWord(secretWord, secretWordUnicode, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    secretWordUnicode: string in Unicode, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    rst = ''
    for i in range(len(secretWord)):
        letter = secretWord[i]
        if letter in lettersGuessed:
            rst += secretWordUnicode[i]
        else:
            rst += '_ '
    return rst



def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    az = list(string.ascii_lowercase)
    for letter in lettersGuessed:
        az.remove(letter)
    return ''.join(az)

    # az = set(az)
    # letters = set(lettersGussed)
    # rst = list(az - letters)
    # return sorted(rst) 

def hangman(secretWord, secretWordUnicode):
    '''
    secretWord: string, the secret word to guess.

    secretWordUnicode : string, the secret word in unicode to guess

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many 
      letters the secretWord contains.

    * Ask the user to supply one guess (i.e. letter) per round.

    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computers word.

    * After each round, you should also display to the user the 
      partially guessed word so far, as well as letters that the 
      user has not yet guessed.

    Follows the other limitations detailed in the problem write-up.
    '''
    print("Welcome to the game, Hangman!")
    print("I am thinking of a word that is {} letters long.".format(str(len(secretWord))))
    mistakes = 0
    lettersGuessed = []

    while(mistakes < 8):
        print("-------------")
        print("You have {} guesses left.".format(str(8 - mistakes)))
        available_letters = getAvailableLetters(lettersGuessed)
        print("Available letters: " + available_letters)
        letter = input("Please guess a letter: ")
        if letter in available_letters:
            lettersGuessed.append(letter)
            guessedWord = getGuessedWord(secretWord, secretWordUnicode, lettersGuessed)
            if letter in secretWord:
                print("Good guess: " + guessedWord)
                if isWordGuessed(secretWord, lettersGuessed):
                    break
            else:
                mistakes += 1
                print("Oops! That letter is not in my word: " + guessedWord)
        else:
            guessedWord = getGuessedWord(secretWord, secretWordUnicode, lettersGuessed)
            print("Oops! You've already guessed that letter: " + guessedWord)


    print("-------------")
    if isWordGuessed(secretWord, lettersGuessed):
        print("Congratulations, you won! The word is '{}'.".format(secretWordUnicode))
    else:
        print("Sorry, you ran out of guesses. The word was {}.".format(secretWordUnicode))


if __name__ == '__main__':
# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
    worddict = loadWords()
    secret = chooseWord(worddict) # secret = ('à', 'a')
    hangman(secret[1], secret[0])
