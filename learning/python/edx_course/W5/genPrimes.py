#!/usr/bin/env python3
import time

def genPrimes():
    x = 2
    primes = set(tuple())
    while True:
        # x is a prime number if (x % p) != 0 for all p where p is a prime number
        if all((x % p) != 0 for p in primes):
            primes.add(x)
            yield x
        x += 1

def genPrimes2():
    primes = []   # primes generated so far
    last = 1      # last number tried
    while True:
        last += 1
        for p in primes:
            if last % p == 0:
                break
        else:
            primes.append(last)
            yield last

t1_start = time.time()
p = genPrimes()
for prime in p:
    if prime > 100000:
        break
t1_end = time.time()
print("genPrimes1 last : {}s".format(t1_end - t1_start))

t2_start = time.time()
p = genPrimes2()
for prime in p:
    if prime > 100000:
        break
t2_end = time.time()
print("genPrimes2 last : {}s".format(t2_end - t1_start))
