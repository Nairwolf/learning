#!/usr/bin/env python3

s = 'azcbobobegghakl'
print(s)

max_seq = 0
final_seq = ''

for i in range(len(s)):
    letter = s[i]
    val = ord(letter)
    seq = letter
    for j in range(i+1, len(s)):
        if ord(s[j]) < val:
            break
        letter = s[j]
        val = ord(letter)
        seq += letter
    print(seq)
    if len(seq) > max_seq:
        max_seq = len(seq)
        final_seq = seq

print("final_seq : "+final_seq)
