#!/usr/bin/env python3

balance = 320000
annualInterestRate = 0.2
monthlyInterestRate = annualInterestRate / 12.0

def balance_year(initBalance, monthlyPayment):
    """
        Compute the balance after one year
    """
    balance = initBalance
    for month in range(12):
        unpaid = balance - monthlyPayment
        balance = unpaid + monthlyInterestRate * unpaid

    return balance

low = balance / 12.0
high = balance*((1 + monthlyInterestRate)**12) / 12.0
middle = (low + high) / 2

annual_balance = balance_year(balance, middle)

while(abs(annual_balance) > 0.01):
    if annual_balance > 0:
        low = middle
    else:
        high = middle

    middle = (low + high) / 2
    annual_balance = balance_year(balance, middle)

if (abs(annual_balance <= 0.01)):
    print("Lowest Payment: "+str(round(middle, 2)))

