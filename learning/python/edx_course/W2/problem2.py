#!/usr/bin/env python3

balance = 3329
annualInterestRate = 0.2
monthlyInterestRate = annualInterestRate / 12.0

def balance_year(initBalance, monthlyPayment):
    """
        Compute the balance after one year
    """
    balance = initBalance
    for month in range(12):
        unpaid = balance - monthlyPayment
        balance = unpaid + monthlyInterestRate * unpaid

    return round(balance, 2)

monthlyPayment = 10
annual_balance = balance_year(balance, monthlyPayment)
while(annual_balance > 0):
    monthlyPayment += 10
    annual_balance = balance_year(balance, monthlyPayment)

if annual_balance <= 0:
    print("Lowest Payment: "+str(monthlyPayment))
