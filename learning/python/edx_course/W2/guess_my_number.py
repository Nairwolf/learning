#!/usr/bin/env python3
print("Please think of a number between 0 and 100!")

start = 0
end = 100
middle = (start + end) // 2

print("Is your secret number "+str(middle)+"?")
answer = input("Enter 'h' to indicate the guess is too high."+
               " Enter 'l' to indicate the guess is too low."+
               " Enter 'c' to indicate I guessed correctly. ")

while (answer != 'c'):
    if answer == 'h':
        end = middle
    elif answer == 'l':
        start = middle
    else:
        print("Sorry, I did not understand your input.")
    
    middle = (start + end) // 2

    print("Is your secret number "+str(middle)+"?")
    answer = input("Enter 'h' to indicate the guess is too high."+
                   " Enter 'l' to indicate the guess is too low."+
                   " Enter 'c' to indicate I guessed correctly. ")

if answer == 'c':
    print("Game over. Your secret number was: "+str(middle))
