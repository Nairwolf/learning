#!/usr/bin/env python3

#balance = 42
#annualInterestRate = 0.2
#monthlyPaymentRate = 0.04

monthlyInterestRate = annualInterestRate / 12.0

for month in range(12):
    minMonthlyPayment = monthlyPaymentRate * balance
    monthlyUnpaidBalance = balance - minMonthlyPayment
    balance = monthlyUnpaidBalance + monthlyInterestRate * monthlyUnpaidBalance 

print("Remaining balance: "+balance)
