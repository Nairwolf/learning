#!/usr/bin/env python3

def flatten(aList):
    ''' 
    aList: a list 
    Returns a copy of aList, which is a flattened version of aList 
    '''
    if len(aList) > 0:
        if type(aList[0]) == type([]):
            return flatten(aList[0]) + flatten(aList[1:])
        else:
            return [aList[0]] + flatten(aList[1:])
    else:
        return aList

if __name__ == '__main__':
    L = [[1,'a',['cat'],2],[[[3]],'dog'],4,5]
    print(flatten(L)) # should be [1,'a','cat',2,3,'dog',4,5]
