#!/usr/bin/env python3

def dict_interdiff(d1, d2):
    '''
    d1, d2: dicts whose keys and values are integers
    Returns a tuple of dictionaries according to the instructions above
    '''
    # Your code here
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())

    intersect_keys = d1_keys & d2_keys
    intersect_dict = {key:f(d1[key], d2[key]) for key in intersect_keys}
    
    diff_keys = d1_keys ^ d2_keys
    diff_dict = {key:d1[key] if key in d1 else d2[key] for key in diff_keys}

    return (intersect_dict, diff_dict)

if __name__ == '__main__':
    def f(a,b):
        rst = a < b
        return rst

    d1 = {1: 0, 2: 1, 3: 2, 4: 3, 5: 0}
    d2 = {1: 1, 2: 2, 3: 3, 4: 5, 6: 2}
    interdiff = dict_interdiff(d1, d2)
    print(interdiff)
