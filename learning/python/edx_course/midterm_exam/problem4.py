#!/usr/bin/env python3

def closest_power(base, num):
    '''
    base: base of the exponential, integer > 1
    num: number you want to be closest to, integer > 0
    Find the integer exponent such that base**exponent is closest to num.
    Note that the base**exponent may be either greater or smaller than num.
    In case of a tie, return the smaller value.
    Returns the exponent.
    '''
    diff = num
    for expo in range(int(num)):
        rst = base ** expo
        last_diff = diff
        diff = abs(rst - num)
        if diff >= last_diff:
            expo -= 1
            break

    return expo

if __name__ == '__main__':
    print(str(closest_power(3, 12)))
    print(str(closest_power(4, 12)))
    print(str(closest_power(4, 1)))

