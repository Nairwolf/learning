#!/usr/bin/env python3

def general_poly (L):
    """ 
        L, a list of numbers (n0, n1, n2, ... nk)
        Returns a function, which when applied to a value x, returns the value 
        n0 * x^k + n1 * x^(k-1) + ... nk * x^0 
    """
    def poly(X):
        polynome = 0
        poow = len(L) - 1
        for coef in L:
            polynome += coef*X**poow
            poow -= 1

        return polynome

    return poly

if __name__ == '__main__':
    L = [1, 2, 3, 4]
    rst = general_poly(L)(10)
    print(rst)
