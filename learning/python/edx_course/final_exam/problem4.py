#!/usr/bin/env python3

def longest_run_old(L):
    """
        Assumes L is a list of integers containing at least 2 elements.
        Finds the longest run of numbers in L, where the longest run can
        either be monotonically increasing or monotonically decreasing. 
        In case of a tie for the longest run, choose the longest run 
        that occurs first.
        Does not modify the list.
        Returns the sum of the longest run. 
    """
    continuous = []
    variation = (L[0] <= L[1])
    last_variation = variation
    i = 0
    nb_continuous_var = 0
    sum_list = L[0]
    while i < len(L) - 1:
        #print("L[{}]={}:L[{}]={}".format(i, L[i], i+1, L[i+1]))
        variation = (L[i] <= L[i+1])
        if variation == last_variation:
            print("L[{}]={}:L[{}]={}".format(i, L[i], i+1, L[i+1]))
            nb_continuous_var += 1
            sum_list += L[i+1]
            print("nb_continous:{}".format(nb_continuous_var))
            print("sum_list:{}".format(sum_list))
        else:
            continuous.append((nb_continuous_var, sum_list))
            nb_continuous_var = 1
            sum_list = L[i] + L[i+1]
            last_variation = variation
        i += 1
    print(continuous)
    s0 = [i[0] for i in continuous]
    s1 = [i[1] for i in continuous]
    return s1[s0.index(max(s0))]

def longest_run(L):
    def longest_run_from_start(l):
        '''
        >>> longest_run_from_start([10, 4, 3, 8, 3, 4, 5])
        [10, 4, 3]
        '''
        ge, le = [l[0]], [l[0]]
        for n in l[1:]:
            if n < ge[-1]: break
            ge.append(n)
        for n in l[1:]:
            if n > le[-1]: break
            le.append(n)
        return max(ge, le, key=len)

    rst_max = max((longest_run_from_start(L[i:]) for i in range(len(L))), key=len)
    return sum(rst_max)

def _cmp(rst, expected):
    if rst == expected:
        print('Ok')
    else:
        print("FAIL: '{}' is not equal to '{}'".format(rst, expected))

if __name__ == '__main__':
    L1 = longest_run([10, 4, 3, 8, 3, 4, 5, 7, 7, 2])
    _cmp(L1, 26)
    L2 = longest_run([5, 4, 10])
    _cmp(L2, 9)
    L3 = longest_run([2, 1, 1, 1, 3])
    _cmp(L3, 5)
