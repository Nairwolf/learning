#!/usr/bin/env python3

trans = {'0':'ling', '1':'yi', '2':'er', '3':'san', '4':'si', '5':'wu', 
        '6':'liu', '7':'qi', '8':'ba', '9':'jiu', '10':'shi'}

def convert_to_mandarin(us_num):
    """
        us_num, a string representing a US number 0 to 99
        returns the string madarin representation of us_num
    """
    num = int(us_num)
    if 0 <= num <= 10:
        return trans[us_num]
    elif 11 <= num <= 19:
        return 'shi ' + trans[us_num[1]]
    elif 20 <= num <= 99:
        if num % 10 == 0:
            return trans[us_num[0]] + ' shi'
        else:
            return trans[us_num[0]] + ' shi ' + trans[us_num[1]]

def _cmp(rst, expected):
    """
        compare rst and expected
        returns True if they're the same, False otherwise
    """
    if rst == expected:
        print("OK")
    else:
        print("FAIL: '{}' is not equal to '{}'".format(rst, expected))

if __name__ == '__main__':
    mandarin_36 = convert_to_mandarin('36')
    _cmp(mandarin_36, 'san shi liu')
    mandarin_20 = convert_to_mandarin('20')
    _cmp(mandarin_20, 'er shi')
    mandarin_16 = convert_to_mandarin('16')
    _cmp(mandarin_16, 'shi liu')

